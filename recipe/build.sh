#!/bin/bash

set -ev

# Settings
mkdir -p \
    "${PREFIX}/etc/conda/activate.d/" \
    "${PREFIX}/etc/conda/deactivate.d/" \
    "${PREFIX}/etc/ipython/profile_default/" \
    "${PREFIX}/etc/jupyter" \
    "${PREFIX}/share/jupyter/runtime"

cat <<EOF > "${PREFIX}/etc/conda/activate.d/jupyterlab_extended.sh"
export IPYTHONDIR=${PREFIX}/etc/ipython
export JUPYTER_CONFIG_DIR=${PREFIX}/etc/jupyter
export JUPYTER_DATA_DIR=${PREFIX}/share/jupyter
export JUPYTER_RUNTIME_DIR=${PREFIX}/share/jupyter/runtime
EOF

cat <<EOF > "${PREFIX}/etc/conda/deactivate.d/jupyterlab_extended.sh"
unset IPYTHONDIR
unset JUPYTER_CONFIG_DIR
unset JUPYTER_DATA_DIR
unset JUPYTER_RUNTIME_DIR
EOF

source "${PREFIX}/etc/conda/activate.d/jupyterlab_extended.sh"

mkdir -p "${PREFIX}/etc/ipython/"
cp "etc/ipython/ipython_config.py" "${PREFIX}/etc/ipython/profile_default/"

# Install packages
python -m pip install --install-option='--skip-npm' -vv ./jupyterlab
python -m pip install -vv ./jupyterlab_code_formatter
python -m pip install -vv ./nbdime
python -m pip install -vv ./jupyterlab_git
python -m pip install -vv ./ipympl
python -m pip install -vv ./ipyvue
python -m pip install -vv ./ipyvuetify
python -m pip install -vv ./jupyter_bokeh

python -m jupyterlab.labextensions install \
    @bokeh/jupyter_bokeh \
    @jupyter-voila/jupyterlab-preview \
    @jupyter-widgets/jupyterlab-manager \
    @jupyterlab/git \
    @jupyterlab/toc \
    @ryantam626/jupyterlab_code_formatter \
    jupyter-vuetify \
    jupyterlab-nvdashboard \
    nbdime-jupyterlab \
    nglview-js-widgets

jupyter serverextension enable jupyterlab_code_formatter --py --sys-prefix
jupyter serverextension enable jupyterlab_git --py --sys-prefix
jupyter serverextension enable nbdime --py --sys-prefix

jupyter nbextension install nbdime --py --sys-prefix
jupyter nbextension install ipympl --py --sys-prefix
jupyter nbextension install jupyter_bokeh --py --sys-prefix

jupyter nbextension enable nbdime --py --sys-prefix
jupyter nbextension enable ipympl --py --sys-prefix
jupyter nbextension enable ipyvue --py --sys-prefix
jupyter nbextension enable ipyvuetify --py --sys-prefix
jupyter nbextension enable jupyter_bokeh --py --sys-prefix

python -m jupyterlab.labapp build

mkdir -p "${PREFIX}/etc/jupyter/lab/user-settings/@jupyterlab/shortcuts-extension/"
cp "user-settings/@jupyterlab/shortcuts-extension/shortcuts.jupyterlab-settings" \
    "${PREFIX}/etc/jupyter/lab/user-settings/@jupyterlab/shortcuts-extension/"

mkdir -p "${PREFIX}/etc/jupyter/lab/user-settings/@ryantam626/jupyterlab_code_formatter/"
cp "user-settings/@ryantam626/jupyterlab_code_formatter/settings.jupyterlab-settings" \
    "${PREFIX}/etc/jupyter/lab/user-settings/@ryantam626/jupyterlab_code_formatter/"
