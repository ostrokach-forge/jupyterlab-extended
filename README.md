# JupyterLab — Extended

## Introduction

This recipe packages JupyterLab with some extensions.

## Settings

See description of environment variables here: <https://jupyter.readthedocs.io/en/latest/use/jupyter-directories.html>.
